import client from "./client";

const register = (pushToken) =>
  client.post("/expPushTokens", { token: pushToken });

export default {
  register,
};
