import colors from "./colors";
import routes from "../navigation/routes";

export default {
  categories: [
    {
      label: "Furniture",
      value: 1,
      backgroundColor: "salmon",
      icon: "floor-lamp",
    },
    {
      label: "Clothing",
      value: 2,
      backgroundColor: "orange",
      icon: "shoe-heel",
    },
    {
      label: "Cameras",
      value: 3,
      backgroundColor: "gold",
      icon: "camera",
    },
    { label: "Cars", value: 4, backgroundColor: "#2ECC40", icon: "car" },
    {
      label: "Books",
      value: 5,
      backgroundColor: "lightseagreen",
      icon: "book-multiple",
    },
    {
      label: "Sports",
      value: 6,
      backgroundColor: "dodgerblue",
      icon: "car-sports",
    },
    {
      label: "Movies & Music",
      value: 7,
      backgroundColor: "orange",
      icon: "headphones",
    },
    {
      label: "Games",
      value: 8,
      backgroundColor: "mediumorchid",
      icon: "gamepad-variant",
    },
    {
      label: "Other",
      value: 9,
      backgroundColor: "slategray",
      icon: "checkbox-multiple-blank-outline",
    },
  ],
  initialMessages: [
    {
      id: 1,
      title: "A very long title that wont display properly.",
      description:
        "A very long description that wont display properly. \
      A very long description that wont display properly. \
      A very long description that wont display properly.",
      image: require("../assets/mosh.jpg"),
    },
    {
      id: 2,
      title: "Title 2",
      description: "Description 2",
      image: require("../assets/mosh.jpg"),
    },
  ],
  listings: [
    {
      id: 1,
      title: "Red jacket for sale",
      price: 100,
      image: require("../assets/jacket.jpg"),
    },
    {
      id: 2,
      title: "Couch in great condition",
      price: 1000,
      image: require("../assets/couch.jpg"),
    },
  ],
  menuItems: [
    {
      title: "My Listings",
      icon: {
        name: "format-list-bulleted",
        backgroundColor: colors.primary,
      },
    },
    {
      title: "My Messages",
      icon: {
        name: "email",
        backgroundColor: colors.secondary,
      },
      targetScreen: routes.MESSAGES,
    },
  ],
};
