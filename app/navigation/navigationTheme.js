import { DefaultTheme, DarkTheme } from "@react-navigation/native";
import colors from "../config/colors";

export default myTheme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: colors.primary,
    text: colors.primary,
    background: colors.white,
    // card: colors.primary,
    // border: colors.primary,
    // notification: colors.primary,
  },
};
